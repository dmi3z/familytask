import { CommonModule } from '@angular/common';
import { BeautyCircleDirective } from './beauty-circle.directive';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [
        BeautyCircleDirective
    ],
    imports: [
        CommonModule
    ],
    exports: [
        BeautyCircleDirective
    ]
})

export class DirectivesModule { }
