import { Directive, ElementRef, Renderer2, OnInit } from '@angular/core';

@Directive({
    selector: '[appBeautyCircle]'
})

export class BeautyCircleDirective implements OnInit {
    constructor(private parent: ElementRef, private renderer: Renderer2) { }

    ngOnInit() {
        this.generateCircles();
    }

    private generateCircles(): void {
        const circlesCount = this.getRandomNumber(5, 10);
        const parentElement = this.parent.nativeElement as HTMLDivElement;
        setTimeout(() => {
            for (let i = 0; i < circlesCount; i++) {
                const elsize = this.getRandomNumber(15, (parentElement.offsetWidth) / 2);
                const circle = this.renderer.createElement('div');
                this.renderer.setStyle(circle, 'position', 'absolute');
                this.renderer.setStyle(circle, 'border-radius', '50%');
                this.renderer.setStyle(circle, 'background', this.getRandomColor());
                this.renderer.setStyle(circle, 'width', elsize + 'px');
                this.renderer.setStyle(circle, 'height', elsize + 'px');
                const top = this.getRandomNumber(-elsize / 2, parentElement.offsetHeight + elsize / 2);
                const left = this.getRandomNumber(-elsize / 2, parentElement.offsetWidth + elsize / 2);
                this.renderer.setStyle(circle, 'top', top + 'px');
                this.renderer.setStyle(circle, 'left', left + 'px');
                this.renderer.setStyle(circle, 'opacity', this.getRandomNumber(2, 10) / 10);
                this.renderer.appendChild(this.parent.nativeElement, circle);
            }
        }, 200);
    }

    private getRandomNumber(min: number, max: number): number {
        const rand = min - 0.5 + Math.random() * (max - min + 1);
        return Math.round(rand);
    }

    private getRandomColor(): string {
        return '#' + (0x1000000 + (Math.random()) * 0xffffff).toString(16).substr(1, 6);
    }

}
