import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AccountPage } from './account.page';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
    declarations: [
        AccountPage
    ],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        DirectivesModule
    ]
})

export class AccountPageModule { }
