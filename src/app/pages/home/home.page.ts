import { Task } from '@models/core';
import { DataService } from '@services/core';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TaskModalComponent } from './task-modal/task-modal.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  public tasks: Task[] = [];

  constructor(private dataService: DataService, private modalController: ModalController) {}

  ngOnInit() {
    this.loadTasks();
  }

  private loadTasks(): void {
    this.dataService.getUserTasks().then(res => {
      this.tasks = res.items;
    }).catch(_ => {
      console.log('Some error');
    });
  }

  async showCreateTaskModal() {
    const modal = await this.modalController.create({
      component: TaskModalComponent
    });
    await modal.present();
    modal.onDidDismiss().then(_ => this.loadTasks());
  }

}
