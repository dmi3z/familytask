import { Task } from '@models/core';
import { LoadingService, DataService } from '@services/core';
import { ModalController, AlertController } from '@ionic/angular';
import { Component } from '@angular/core';
import * as moment from 'moment';

@Component({
    selector: 'app-task-modal',
    templateUrl: 'task-modal.component.html',
    styleUrls: ['task-modal.component.scss']
})

export class TaskModalComponent {

    public name: string;
    public text: string;
    private datetime: number;

    constructor(
        private modalController: ModalController,
        private dataService: DataService,
        private loadinService: LoadingService,
        private alertController: AlertController
    ) { }

    public closeModal() {
        this.modalController.dismiss({
            dismissed: true
        });
    }

    public onDateChange(event: CustomEvent) {
        this.datetime = moment(event.detail.value).unix();
    }

    public async createTask() {
        this.loadinService.startLoading();
        if (this.name.length > 0 && this.text.length > 0 && this.datetime > 0) {
            const task: Task = {
                is_complete: false,
                name: this.name,
                text: this.text,
                communicate: [],
                expired: this.datetime,
                created: moment().unix(),
            };
            this.dataService.saveTask(task).then(_ => {
                this.loadinService.stopLoading();
                this.closeModal();
            });
        } else {
            this.showAlert();
        }
    }

    private async showAlert() {
        const alert = await this.alertController.create({
            header: 'Ошибочка',
            subHeader: 'Не все поля заполнены',
            message: 'Для создания заметки необходимо заполнить все поля.',
            buttons: ['OK']
        });
        await alert.present();
    }
}
