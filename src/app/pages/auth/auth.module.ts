import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AuthPage } from './auth.page';
import { DirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
    declarations: [
        AuthPage
    ],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        DirectivesModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            {
                path: '',
                component: AuthPage
            }
        ])
    ]
})

export class AuthPageModule {}
