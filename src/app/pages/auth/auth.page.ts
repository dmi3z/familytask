import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '@services/core';
import { User } from '@models/core';
import { AlertController } from '@ionic/angular';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-auth-page',
  templateUrl: 'auth.page.html',
  styleUrls: ['auth.page.scss']
})

export class AuthPage implements OnInit {

  public email: FormControl;
  public password: FormControl;

  public authForm: FormGroup;

  constructor(
    private authService: AuthService,
    private router: Router,
    private alertController: AlertController,
    public loadingService: LoadingService
  ) { }

  ngOnInit() {
    this.createFormFields();
    this.createAuthForm();
  }

  public login(): void {
    if (this.authForm.valid) {
      const user: User = {
        email: this.email.value,
        password: this.password.value
      };
      this.loadingService.startLoading();
      this.authService.login(user).then(res => {
        if (res) {
          this.router.navigate(['home']);
        } else {
          this.presentAlert();
        }
        this.loadingService.stopLoading();
      });
    }
  }

  private async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Ой!',
      subHeader: 'Ошибка авторизации.',
      message: 'Email или пароль введены неверно.',
      buttons: ['OK']
    });

    await alert.present();
  }

  private createFormFields(): void {
    this.email = new FormControl('', [Validators.required, Validators.email, Validators.minLength(6), Validators.maxLength(25)]);
    this.password = new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(15)]);
  }

  private createAuthForm(): void {
    this.authForm = new FormGroup({
      email: this.email,
      password: this.password
    });
  }
}
