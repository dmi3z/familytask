export interface User {
    id?: number;
    name?: string;
    email: string;
    password: string;
    task_list_id?: number[];
}
