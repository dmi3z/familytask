export interface Task {
    id?: number;
    name: string;
    text: string;
    created: number;
    expired: number;
    is_complete: boolean;
    communicate: number[];
}

export interface TaskList {
    id: number;
    items: Task[];
}
