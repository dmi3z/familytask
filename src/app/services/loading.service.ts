import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable()
export class LoadingService {

    private loading: any;

    constructor(private loadingController: LoadingController) { }

    async startLoading() {
        this.loading = await this.loadingController.create({
            message: 'Подождите',
        });
        this.loading.present();
    }

    stopLoading(): void {
        setTimeout(() => {
            this.loading.dismiss();
        }, 500);
    }

}
