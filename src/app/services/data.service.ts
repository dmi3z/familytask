import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TaskList, Task } from '@models/core';

@Injectable()
export class DataService {

    private readonly URL = 'http://dmi3z.herokuapp.com/';

    constructor(private http: HttpClient) {}

    getUserTasks(): Promise<TaskList> {
        return this.http.get<TaskList>(this.URL.concat('tasks')).toPromise();
    }

    getTaskInfo(taskId: number): Promise<Task> {
        return this.http.get<Task>(this.URL.concat('tasks/', taskId.toString())).toPromise();
    }

    saveTask(body: Task): Promise<any> {
        return this.http.post(this.URL.concat('tasks'), body).toPromise();
    }
}
