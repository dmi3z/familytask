import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from '@models/core';

@Injectable()

export class AuthService {

  private readonly url = 'http://dmi3z.herokuapp.com/';

  constructor(private router: Router, private http: HttpClient) { }

  get isAuth(): boolean {
    const isAuth = localStorage.getItem('familytoken');
    if (isAuth) {
      return true;
    }
    return false;
  }

  get token(): number {
    return Number(localStorage.getItem('familytoken'));
  }

  login(user: User): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.http.post<number>(this.url.concat('auth'), user).toPromise().then(res => {
        localStorage.setItem('familytoken', res.toString());
        resolve(true);
      }).catch(_ => {
        resolve(false);
      });
    });
  }

  logout(): void {
    localStorage.removeItem('familytoken');
    this.router.navigate(['auth']);
  }

}
